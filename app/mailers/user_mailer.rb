class UserMailer < ActionMailer::Base
  default from: "kpi.kanini@gmail.com"
  def class_completion_email(bhumi_class)
    @class = bhumi_class
    recipient_id = Volunteer.find(@class.filled_by).email
    mail(to: recipient_id, subject: 'Bhumi Class details saved!')
  end

  def attendance_reminder_email(bhumi_class)
    @class = bhumi_class
    @recipients = @class.center.mailing_list
    mail(to: @recipients, subject: 'Please fill attendance!')
  end
  
  def reminder_email
    recipients = Volunteer.mail_recipients
    mail(to: recipients, subject: 'Please fill log for Bhumi Class')
  end

  def log_update_status_email 
    admins = ["supu.berry@gmail.com", "vigneshvar.bhumi@gmail.com"]
    mail(to: admins, subject: 'Log sheet update status')
  end
  
end
