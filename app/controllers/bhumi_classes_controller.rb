class BhumiClassesController < ApplicationController
	before_filter :auth_user
	# before_filter :vol_allowed
	def create
		@center = Center.find(params[:center_id])
		@chapters = Chapter.all.collect{|c| c.name}
		@sub_chapters = SubChapter.all.collect{|s| s.name}

		unless (@center.bhumi_classes.find_by_date(params[:bhumi_class][:date])!= nil)
			if (params[:bhumi_class][:date].to_date.strftime("%A").downcase == 'saturday' || params[:bhumi_class][:date].to_date.strftime("%A").downcase == 'sunday')
				@class = @center.bhumi_classes.create(:date => params[:bhumi_class][:date], :day_of_week => params[:bhumi_class][:date].to_date.strftime("%A"),:no_of_systems => params[:bhumi_class][:no_of_systems],
													:conducted_status => params[:bhumi_class][:conducted_status], :chapter_taught => params[:bhumi_class][:chapter_taught],
													:sub_chapter_taught => params[:bhumi_class][:sub_chapter_taught], :reason_for_cancellation => params[:bhumi_class][:reason_for_cancellation],
													:comment => params[:bhumi_class][:comment], :filled_by => params[:bhumi_class][:filled_by])


				if @class.save
					UserMailer.class_completion_email(@class).deliver
					if (params[:bhumi_class][:conducted_status] == "0")
						flash.notice = "Successfully updated"
						redirect_to @center
					else
						flash.notice = "Successfully updated"
						redirect_to @class
						@class.send_attendance_reminder
					end
				else
					redirect_to :back
				end
			else
				flash.alert = "Selected date is not a weekend"
				redirect_to :back
			end
		else
			@conducted_class ||= @center.bhumi_classes.where('date = (?) AND conducted_status = (?)',params[:bhumi_class][:date],1)
			@cancelled_class ||= @center.bhumi_classes.where('date = (?) AND conducted_status = (?)',params[:bhumi_class][:date],0)
			if (@conducted_class.count!=0 || @cancelled_class!=0)
				if(@conducted_class.count!=0)
					redirect_to bhumi_class_path(@conducted_class.first.id, :count=>@conducted_class.count)
				else #if(@cancelled_class!=0)
					redirect_to bhumi_class_path(@cancelled_class.first.id, :count=>@cancelled_class.count)
				end
			end
		end
	end

	def show
		if params[:id]
		@class = BhumiClass.find(params[:id])
		else
		@class = current_class		
		end

		if params[:count]
                @count = params[:count]
                else
                @count = ""
                end

		@center = @class.center
                @volunteers = Volunteer.center_vols(@center.id)
                @students = Student.center_students(@center.id)
	end	

	def update_sub_chapters
		@center = Center.find(params[:center])
                subchapter = SubChapter.where(:chapter_id=>params[:id]) unless params[:id].blank?
                render :partial => "bhumi_classes/sub_chapters", :locals => { :subchapter => subchapter}
    	end

end


