class CentersController < ApplicationController
	before_filter :auth_user
	before_filter :auth_center
	def show
		if params[:id]
			@center = Center.find(params[:id])
		else
			@center = current_center
		end
	end
end
