class OmniauthCallbacksController < ApplicationController
	def google_oauth2
		auth=request.env["omniauth.auth"]
                volunteer = Volunteer.from_omniauth(auth)
		#volunteer = Volunteer.from_omniauth(request.env["omniauth.auth"])
		if volunteer != nil
			if volunteer.persisted?
				flash.notice = "Signed in Through Google!"
				sign_in  volunteer
				session[:volunteer_id] = volunteer.id
				# redirect_to volunteer_path(volunteer.id)
				if (volunteer.batch_coordinator == true)
					center = Center.find(volunteer.center_id)
					redirect_to center_path(center.id)
				else
					redirect_to volunteer_path(volunteer.id)
				end
			else
				flash.alert = "Error in signing in!!"
		    		redirect_to root_path
				# session["devise.volunteer_attributes"] = volunteer.attributes
				# flash.notice = "Signed in successfully!"
				# sign_in  volunteer
				# redirect_to volunteer_path(volunteer.id)
			end
		else
			flash.alert = "Your email id " + auth.info.email + " is not registered with Bhumi"
                        redirect_to root_path
		end
	end
	def failure
		flash.alert = "Error in signing in!!"
    	redirect_to root_path
    #redirect wherever you want.
  	end
end
