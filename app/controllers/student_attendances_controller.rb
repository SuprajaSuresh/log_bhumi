class StudentAttendancesController < ApplicationController
	before_filter :auth_user
	# before_filter :vol_allowed
	def create
		# 0 - Failed
                # 1 - Success
                # 2 - Already Exist
                success=1
		stud_attendance = (params[:student_attendances] || {}).values
        	stud_attendance.each do |attendance|
        		c_id = attendance[:bhumi_class_id].to_i
		        s_id = attendance[:student_id].to_i
		        status = attendance[:attendance_status]
			alreadyrecorded = StudentAttendance.where(:bhumi_class_id => c_id, :student_id => s_id)
                        if alreadyrecorded.empty?
			        att = StudentAttendance.create( :bhumi_class_id => c_id,
			           :student_id => s_id, :attendance_status => status )
				if not att.save
                                        success=0
                                end

			else
                                success=2
                        end

		end
		if success==1
                        flash.notice="Student attendance succesfully saved"
                        redirect_to :back
                        return
                elsif success==0
                        flash.alert="Failed to save Student Attendance"
                        redirect_to :back
                        return
                else
                        flash.notice="Already saved Student attendance"
                        redirect_to :back
                        return
                end
	end
end
