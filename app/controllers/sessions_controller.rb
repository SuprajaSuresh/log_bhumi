class SessionsController < ApplicationController

	def new
	end

	def destroy
		@session = Session.find(params[:id])
		@session[:volunteer_id] = nil
		@session.destroy
		flash.notice = "Signed out"
		redirect_to root_path
	end
end
