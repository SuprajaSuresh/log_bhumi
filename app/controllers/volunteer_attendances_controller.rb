class VolunteerAttendancesController < ApplicationController
	before_filter :auth_user
	# before_filter :vol_allowed
	def create
		# 0 - Failed
		# 1 - Success
		# 2 - Already Exist
		success=1
		vol_attendance = (params[:volunteer_attendances] || {}).values
        	vol_attendance.each do |attendance|
        		c_id = attendance[:bhumi_class_id].to_i
		        v_id = attendance[:volunteer_id].to_i
		        status = attendance[:attendance_status]
			alreadyrecorded = VolunteerAttendance.where( :bhumi_class_id => c_id, :volunteer_id => v_id)
			if alreadyrecorded.empty?
		        	att = VolunteerAttendance.create( :bhumi_class_id => c_id,
		           	:volunteer_id => v_id, :attendance_status => status )
				if not att.save
					success=0
	                	end
			else
				success=2
			end
		end
		if success==1
			flash.notice="Volunteer attendance succesfully saved"
                        redirect_to :back
                        return
		elsif success==0
			flash.alert="Failed to save Volunteer Attendance"
			redirect_to :back
                        return
		else
			flash.notice="Already saved volunteer attendance"
			redirect_to :back
                        return
		end
	end
end
