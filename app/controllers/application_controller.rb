class ApplicationController < ActionController::Base
	protect_from_forgery

	def auth_user
    	redirect_to root_url ,:notice => "Please sign in" unless volunteer_signed_in?
  	end

  	def auth_center
  		@volunteer = current_volunteer
		if params[:id]
  			@center = Center.find(params[:id])
		else
			@center = current_center
		end
  		redirect_to root_url ,:notice => " Access denied " unless ((@volunteer.center_id == @center.id) && 
  							(@volunteer.batch_coordinator == true || @volunteer.center_coordinator == true))
  	end
 
	private
	    def current_volunteer
	        @current_volunteer ||= Volunteer.find(session[:volunteer_id]) if session[:volunteer_id]
	    end

	    def current_center
			@current_center ||= Center.find(Volunteer.find(session[:volunteer_id]).center_id) if session[:volunteer_id]
	    end
end
