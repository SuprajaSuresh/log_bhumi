class VolunteersController < ApplicationController
	before_filter :auth_user
	# before_filter :vol_allowed
	def show
		if params[:id]
			@vol = Volunteer.find(params[:id])
		else
			@vol = current_volunteer
		end
		@zones = Zone.all.collect{|z| [z.name,z.id]}
	end

	def update_centers
	    # updates centers based on zone selected
	    zone = Zone.find(params[:zone_id])
	    # map to name and id for use in our options_for_select
	    @centers = zone.centers.map{|c| [c.name, c.id]}.insert(0, "Choose your center")
  	end

  	def center_redirect
	    center = Center.find(params[:center_id])
	    render :js => "window.location =  '#{center_path(center.id)}'"
  	end

end
