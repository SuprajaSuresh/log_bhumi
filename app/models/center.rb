class Center < ActiveRecord::Base
	has_many :students
	has_many :volunteers
	belongs_to :zone
	has_many :bhumi_classes
	attr_accessible :address_line_1, :address_line_2, :cc_name, :city, :day_of_week, :name, :saturday_bc_name, :state, :sunday_bc_name, :zc_name, :zone_name

	def saturday_filling_status
		unless self.day_of_week == "sunday"
			unless self.bhumi_classes.find_by_date(Date.today.beginning_of_week(:saturday)).nil?
				return "Filled"
			else
				return "Not Filled"
			end
		else
			return "NA"
		end
	end

	def sunday_filling_status
		unless self.day_of_week == "saturday"
			unless self.bhumi_classes.find_by_date(Date.today.beginning_of_week(:sunday)).nil?
				return "Filled"
			else
				return "Not Filled"
			end
		else
			return "NA"
		end
	end

	def mailing_list
		recipients_array = []
		recipients_array.push(self.cc_id,self.saturday_bc_id,self.sunday_bc_id,self.zc_id).uniq()
		recipients_email = Volunteer.where(:id => recipients_array).pluck(:email)
	end
end
