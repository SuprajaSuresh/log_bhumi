class Zone < ActiveRecord::Base
	has_many :centers
  	attr_accessible :name, :zc_name, :city
end
