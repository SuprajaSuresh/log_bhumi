class SubChapter < ActiveRecord::Base
	belongs_to :chapter
  	attr_accessible :name, :number, :chapter_id
end
