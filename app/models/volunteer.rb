class Volunteer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauth_providers => [:google_oauth2]

  belongs_to :center
  has_many :volunteer_attendances
  has_many :bhumi_classes, :through => :volunteer_attendances
  
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, 
  				  :first_name, :last_name, :mobile, :date_of_birth, 
  				  :date_of_joining, :center, :no_of_classes_present, 
  				  :no_of_classes_absent, :no_of_leaves, :status, :function, 
  				  :day_of_week, :is_admin, :kpi_member, :hr_member, 
  				  :zone_coordinator, :center_coordinator,:batch_coordinator

  # attr_accessible :title, :body

  def self.from_omniauth(auth)
    if volunteer = Volunteer.find_by_email(auth.info.email)
      volunteer.provider = auth.provider
#      volunteer.email = auth.auth
      volunteer
    #else
    #  where(auth.slice(:provider, :email)).first_or_create do |volunteer|
    #    volunteer.provider = auth.provider
        # volunteer.name = auth.info.name
    #    volunteer.email = auth.info.email
    #  end
    end
  end

  def self.center_vols(c_id)
    self.where(:center_id => c_id )
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def check_position
    self.batch_coordinator == true || self.zone_coordinator == true || self.center_coordinator == true || self.kpi_member == true
  end

  def self.send_reminder
    UserMailer.reminder_email.deliver
  end

  def self.mail_recipients
    Volunteer.where("center_coordinator = ?  OR batch_coordinator =? ",1,1).pluck(:email)
  end
end
