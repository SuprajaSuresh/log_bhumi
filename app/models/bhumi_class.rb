class BhumiClass < ActiveRecord::Base
	has_many :volunteer_attendances
  	has_many :volunteers, :through => :volunteer_attendances
  	has_many :student_attendances
	has_many :students, :through => :student_attendances
	belongs_to :center
	has_many :chapters
	has_many :sub_chapters
	attr_accessible :chapter_taught, :comment, :conducted_status, :date, :day_of_week, :no_of_students, :no_of_systems, :no_of_volunteers, :reason_for_cancellation, :sub_chapter_taught, :filled_by

	def when_to_remind
		self.created_at + 2.days
	end

	def send_attendance_reminder
		if(VolunteerAttendance.where(:bhumi_class_id => self.id).empty?) || 
			(StudentAttendance.where(:bhumi_class_id => self.id).empty?)
			UserMailer.attendance_reminder_email(self).deliver
		end
	end
	handle_asynchronously :send_attendance_reminder, :run_at => Proc.new {|i| i.when_to_remind} 
end
