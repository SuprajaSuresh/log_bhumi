class Student < ActiveRecord::Base
	belongs_to :center
	has_many :student_attendances
  	has_many :bhumi_classes, :through => :student_attendances
  	attr_accessible :center, :date_of_birth, :first_name, :last_name, :no_of_classes_absent, :no_of_classes_present, :school, :standard

  	def self.center_students(c_id)
    	self.where(:center_id => c_id )
  	end

	def full_name
    		"#{first_name} #{last_name}"
  	end
end
