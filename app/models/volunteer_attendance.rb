class VolunteerAttendance < ActiveRecord::Base
	belongs_to :volunteer 
	belongs_to :bhumi_class
  	attr_accessible :volunteer_id, :bhumi_class_id , :attendance_status
end
