LogBhumi::Application.routes.draw do
  # devise_for :volunteers
  devise_for :volunteers, controllers: { omniauth_callbacks: "omniauth_callbacks" }

  resources :volunteers do
    collection do
      get 'update_centers'
      get 'center_redirect'
    end
  end
  resources :volunteer_attendances
  resources :student_attendances

  resources :centers do
    resources :bhumi_classes
  end

  resources :bhumi_classes do
    resources :volunteer_attendances
    resources :student_attendances
  end
  
  get 'volunteers/update_centers', :as => 'update_centers'
  get 'volunteers/center_redirect', :as => 'center_redirect'
  match 'bhumi_classes/update_sub_chapters/:id', :controller=>'bhumi_classes', :action => 'update_sub_chapters'
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  authenticated :volunteer do
  	root :to => 'centers#show'
  end
  root :to => 'sessions#new'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
