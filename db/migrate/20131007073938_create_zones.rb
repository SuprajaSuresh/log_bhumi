class CreateZones < ActiveRecord::Migration
  def change
    create_table :zones do |t|
      t.string :name
      t.string :zc_name
      t.string :city

      t.timestamps

      # add_index :zones, :name,                :unique => true
    end
  end
end
