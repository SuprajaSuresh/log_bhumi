class DeviseCreateVolunteers < ActiveRecord::Migration
  def change
    create_table(:volunteers) do |t|
      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      t.string   :provider

      ## Confirmable
      # t.string   :confirmation_token
      # t.datetime :confirmed_at
      # t.datetime :confirmation_sent_at
      # t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, :default => 0, :null => false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at


      t.string :first_name
      t.string :last_name
      t.integer :mobile
      t.date :date_of_birth
      t.date :date_of_joining
      t.integer :no_of_classes_present
      t.integer :no_of_classes_absent
      t.integer :no_of_leaves
      t.string :status
      t.string :function
      t.string :day_of_day_of_week
      t.boolean :is_admin
      t.boolean :kpi_member
      t.boolean :hr_member
      t.boolean :zone_coordinator
      t.boolean :center_coordinator
      t.boolean :batch_coordinator
      t.boolean :project_coordinator

      t.timestamps
      t.references :center
      t.references :class
    end

    # add_index :volunteers, :email,                :unique => true
    # add_index :volunteers, :reset_password_token, :unique => true
    # add_index :volunteers, :confirmation_token,   :unique => true
    # add_index :volunteers, :unlock_token,         :unique => true
  end
end
