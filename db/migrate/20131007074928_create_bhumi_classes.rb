class CreateBhumiClasses < ActiveRecord::Migration
  def change
    create_table :bhumi_classes do |t|
      t.date :date
      t.string :day_of_week
      t.integer :no_of_volunteers
      t.integer :no_of_students
      t.integer :no_of_systems
      t.boolean :conducted_status
      t.string :reason_for_cancellation
      t.string :comment
      t.string :chapter_taught
      t.string :sub_chapter_taught

      t.timestamps
      t.references :center
    end
  end
end
