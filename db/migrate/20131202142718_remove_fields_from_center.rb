class RemoveFieldsFromCenter < ActiveRecord::Migration
  def up
  	remove_column :centers, :cc_name
  	remove_column :centers, :saturday_bc_name
  	remove_column :centers, :sunday_bc_name
  	remove_column :centers, :zc_name
  end

  def down
  end
end
