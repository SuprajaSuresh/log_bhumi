class CreateCenters < ActiveRecord::Migration
  def change
    create_table :centers do |t|
      t.string :name
      t.string :zone_name
      t.string :cc_name
      t.string :saturday_bc_name
      t.string :sunday_bc_name
      t.string :zc_name
      t.string :address_line_1
      t.string :address_line_2
      t.string :city
      t.string :state
      t.string :day_of_week

      t.timestamps

      t.references :zone
    end
  end
end
