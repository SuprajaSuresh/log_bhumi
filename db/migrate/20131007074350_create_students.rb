class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.date :date_of_birth
      t.integer :standard
      t.string :center
      t.string :school
      t.integer :no_of_classes_present
      t.integer :no_of_classes_absent

      t.timestamps
      t.references :center
      t.references :class
    end
  end
end
