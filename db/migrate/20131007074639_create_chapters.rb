class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.string :name
      t.integer :no_of_hours_required

      t.timestamps
      t.references :bhumi_class
    end
  end
end
