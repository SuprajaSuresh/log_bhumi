class CreateStudentAttendances < ActiveRecord::Migration
  def change
    create_table :student_attendances do |t|

	  t.string :attendance_status

      t.references :student
      t.references :bhumi_class

      t.timestamps
    end
  end
end
