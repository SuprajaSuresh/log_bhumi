class CreateSubChapters < ActiveRecord::Migration
  def change
    create_table :sub_chapters do |t|
      t.float :number
      t.string :name

      t.timestamps
      t.references :chapter
      t.references :bhumi_class

      # add_index :subchapters, :number,                :unique => true
    end
  end
end
