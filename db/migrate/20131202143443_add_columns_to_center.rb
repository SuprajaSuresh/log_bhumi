class AddColumnsToCenter < ActiveRecord::Migration
  def change
  	add_column :centers, :cc_id, :integer
  	add_column :centers, :saturday_bc_id, :integer
  	add_column :centers, :sunday_bc_id, :integer
  	add_column :centers, :zc_id, :integer
  end
end
