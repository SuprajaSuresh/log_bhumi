class CreateVolunteerAttendances < ActiveRecord::Migration
  def change
    create_table :volunteer_attendances do |t|

      t.string :attendance_status

      t.references :volunteer
      t.references :bhumi_class
      t.timestamps
    end
  end
end
