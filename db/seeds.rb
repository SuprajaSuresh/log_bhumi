# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Populating State Data
chapters = Chapter.create([
                        {:name => 'INTRODUCTION TO COMPUTERS', :no_of_hours_required => 3},
                        {:name => 'INTRODUCTION TO WINDOWS', :no_of_hours_required => 3},
                        {:name => 'PAINT', :no_of_hours_required => 3},
                        {:name => 'MS WORD', :no_of_hours_required => 3},
                        {:name => 'MS POWERPOINT', :no_of_hours_required => 3},
                        {:name => 'MS EXCEL', :no_of_hours_required => 3},
                        {:name => 'INTERNET', :no_of_hours_required => 3},
                        {:name => 'EMAIL', :no_of_hours_required => 3}
                      ])
sub_chapters = SubChapter.create([
                        {:number => 1.1, :name => 'What is a computer', :chapter_id => 1},
                        {:number => 1.2, :name => 'Uses of computer', :chapter_id => 1},
                        {:number => 1.3, :name => 'History of a computer', :chapter_id => 1},
                        {:number => 1.4, :name => 'Parts of a computer', :chapter_id => 1},
                        {:number => 1.5, :name => 'Binary system - How we talk to computers', :chapter_id => 1},
                        {:number => 1.6, :name => 'Dos and Donts in a computer', :chapter_id => 1},
                        {:number => 1.7, :name => 'Computer and health', :chapter_id => 1},

                        {:number => 2.1, :name => 'What is windows', :chapter_id => 2},
                        {:number => 2.2, :name => 'Intro to Window', :chapter_id => 2},
                        {:number => 2.3, :name => 'How to Maximize', :chapter_id => 2},
                        {:number => 2.4, :name => 'Icons', :chapter_id => 2},
                        {:number => 2.5, :name => 'What is a file', :chapter_id => 2},
                        {:number => 2.6, :name => 'How to set the wallpaper', :chapter_id => 2},
                        {:number => 2.7, :name => 'Screensaver', :chapter_id => 2},
                        {:number => 2.8, :name => 'Date', :chapter_id => 2},

                        {:number => 3.1, :name => 'Shapes and Colors', :chapter_id => 3},
                        {:number => 3.2, :name => 'Coloring options', :chapter_id => 3},
                        {:number => 3.3, :name => 'Text editing', :chapter_id => 3},
                        {:number => 3.4, :name => 'Image editing', :chapter_id => 3},
                        {:number => 3.5, :name => 'Final Asessment', :chapter_id => 3},
                  		
                        {:number => 4.1, :name => 'Text Content', :chapter_id => 4},
                        {:number => 4.2, :name => 'Formatting Content', :chapter_id => 4},
                        {:number => 4.3, :name => 'Graphical Content', :chapter_id => 4},
                        {:number => 4.4, :name => 'Table', :chapter_id => 4},
                  		
                  		{:number => 5.1, :name => 'Introduction to Power point', :chapter_id => 5},
                        {:number => 5.2, :name => 'Creating a presentation', :chapter_id => 5},
                        {:number => 5.3, :name => 'Slide Show', :chapter_id => 5},
                        {:number => 5.1, :name => 'Formatting Slides', :chapter_id => 5},
                        {:number => 5.5, :name => 'Views', :chapter_id => 5},
                        {:number => 5.6, :name => 'Animation', :chapter_id => 5},

                        {:number => 6.1, :name => 'Basics of excel', :chapter_id => 6},
                        {:number => 6.2, :name => 'Editing excel', :chapter_id => 6},
                        {:number => 6.3, :name => 'Formatting worksheets', :chapter_id => 6},
                        {:number => 6.1, :name => 'Managing worksheets and workbooks', :chapter_id => 6},
                        {:number => 6.5, :name => 'View Control', :chapter_id => 6},
                        {:number => 6.6, :name => 'Excel Formula', :chapter_id => 6},
                        {:number => 6.7, :name => 'Charts', :chapter_id => 6}
                        
                      ])