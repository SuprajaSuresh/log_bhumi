# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131204105321) do

  create_table "bhumi_classes", :force => true do |t|
    t.date     "date"
    t.string   "day_of_week"
    t.integer  "no_of_volunteers"
    t.integer  "no_of_students"
    t.integer  "no_of_systems"
    t.boolean  "conducted_status"
    t.string   "reason_for_cancellation"
    t.string   "comment"
    t.string   "chapter_taught"
    t.string   "sub_chapter_taught"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "center_id"
    t.integer  "filled_by"
  end

  create_table "centers", :force => true do |t|
    t.string   "name"
    t.string   "zone_name"
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "city"
    t.string   "state"
    t.string   "day_of_week"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "zone_id"
    t.integer  "cc_id"
    t.integer  "saturday_bc_id"
    t.integer  "sunday_bc_id"
    t.integer  "zc_id"
  end

  create_table "chapters", :force => true do |t|
    t.string   "name"
    t.integer  "no_of_hours_required"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "bhumi_class_id"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "student_attendances", :force => true do |t|
    t.string   "attendance_status"
    t.integer  "student_id"
    t.integer  "bhumi_class_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "students", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.integer  "standard"
    t.string   "center"
    t.string   "school"
    t.integer  "no_of_classes_present"
    t.integer  "no_of_classes_absent"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "center_id"
    t.integer  "class_id"
  end

  create_table "sub_chapters", :force => true do |t|
    t.float    "number"
    t.string   "name"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "chapter_id"
    t.integer  "bhumi_class_id"
  end

  create_table "volunteer_attendances", :force => true do |t|
    t.string   "attendance_status"
    t.integer  "volunteer_id"
    t.integer  "bhumi_class_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "volunteers", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "mobile"
    t.date     "date_of_birth"
    t.date     "date_of_joining"
    t.integer  "no_of_classes_present"
    t.integer  "no_of_classes_absent"
    t.integer  "no_of_leaves"
    t.string   "status"
    t.string   "function"
    t.string   "day_of_day_of_week"
    t.boolean  "is_admin"
    t.boolean  "kpi_member"
    t.boolean  "hr_member"
    t.boolean  "zone_coordinator"
    t.boolean  "center_coordinator"
    t.boolean  "batch_coordinator"
    t.boolean  "project_coordinator"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.integer  "center_id"
    t.integer  "class_id"
  end

  create_table "zones", :force => true do |t|
    t.string   "name"
    t.string   "zc_name"
    t.string   "city"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
